import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.*;

public class Camera {
    Socket server;
    HttpURLConnection connection;
    BufferedInputStream httpIn;
    BufferedOutputStream out;

    public Camera() {
    }

    public void connectTo(InetAddress ip, int port) throws IOException {
        server = new Socket(ip, port);
        URL url = new URL("http://89.203.137.209/mjpg/video.mjpg");
        connection = (HttpURLConnection) url.openConnection();
        httpIn = new BufferedInputStream(connection.getInputStream());
        out = new BufferedOutputStream(server.getOutputStream());
    }

    public void stream() throws IOException {
        while (true){
            out.write(httpIn.read());
        }
    }

}
