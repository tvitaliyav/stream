import java.io.IOException;
import java.net.InetAddress;

public class AppCamera {
    public static void main(String[] args) throws IOException, InterruptedException {
        Camera camera = new Camera();
        camera.connectTo(InetAddress.getLocalHost(), 6060);
        camera.stream();
    }
}
