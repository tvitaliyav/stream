import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.net.InetAddress;

public class AppClient {

    public static void main(String[] args) throws IOException, InterruptedException {
        Client client = new Client();
        client.connectTo(InetAddress.getLocalHost(), 6061);
        Desktop desktop = Desktop.getDesktop();
        File html = new File("D:\\University\\5 year\\TPZRP\\STREAM\\stream_client\\src\\main\\java\\Client.html");
        desktop.open(html);
        client.stream();
    }
}
