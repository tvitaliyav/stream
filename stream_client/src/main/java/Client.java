import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.InetAddress;
import java.net.Socket;

public class Client {
    Socket server;
    BufferedInputStream out;

    public void connectTo(InetAddress ip, int port) throws IOException{
        server = new Socket(ip, port);
        out = new BufferedInputStream(server.getInputStream());
    }

    public void stream() throws IOException {
        while (true){
            byte [] b = new byte[out.available()];
            out.read(b);
            BufferedImage img = ImageIO.read(new ByteArrayInputStream(b));
            String path = "D:\\University\\5 year\\TPZRP\\STREAM\\stream_client\\img.jpg";
            File outputFile = new File(path);
            if(img != null) {
                ImageIO.write(img, "jpg", outputFile);
            }
        }
    }

}
