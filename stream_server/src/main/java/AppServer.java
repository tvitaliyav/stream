import nu.pattern.OpenCV;
import org.opencv.core.Core;

public class AppServer {

    public static void main(String[] args){
        OpenCV.loadShared();
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
        Server camera = new Server();
        camera.run();
    }
}
