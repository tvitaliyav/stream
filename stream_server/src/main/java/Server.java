import org.opencv.core.Mat;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;


public class Server {

    private ServerSocket ServerCamera;
    private Socket connectedCamera;
    private BufferedInputStream serverIn;
    private ServerSocket ServerClient;
    private Socket connectedClient;
    private BufferedOutputStream serverOut;

    public Server() {

    }

    public void connect(int port1, int port2) throws IOException {
        ServerCamera = new ServerSocket(port1);
        System.out.println("[Camera] Waiting for connection");
        connectedCamera = ServerCamera.accept();
        System.out.println("[Camera] Connect");
        serverIn = new BufferedInputStream(connectedCamera.getInputStream());
        ServerClient = new ServerSocket(port2);
        System.out.println("[Client] Waiting for connection");
        connectedClient = ServerClient.accept();
        System.out.println("[Client] Connect");
        serverOut = new BufferedOutputStream(connectedClient.getOutputStream());
    }


    public void getImg() throws IOException {
        System.out.println("[Camera] Get image start");
        byte[] curFrame = null;
        boolean frameAvailable = false;
        ByteArrayOutputStream jpgOut = null;

        int prev = 0;
        int cur = 0;
        while (true) {
            try {
                while (serverIn != null && (cur = serverIn.read()) >= 0 && !frameAvailable) {
                    if (prev == 0xFF && cur == 0xD8) {
                        jpgOut = new ByteArrayOutputStream(8192);
                        jpgOut.write((byte) prev);
                    }
                    if (jpgOut != null) {
                        jpgOut.write((byte) cur);
                    }
                    if (prev == 0xFF && cur == 0xD9) {
                        assert jpgOut != null;
                        curFrame = jpgOut.toByteArray();
                        frameAvailable = true;
                    }
                    prev = cur;
                }
            } catch (IOException e) {
                System.err.println("I/O Error: " + e.getMessage());
            }

            assert curFrame != null;
            ByteArrayInputStream jpgIn = new ByteArrayInputStream(curFrame);
            BufferedImage bufImg = ImageIO.read(jpgIn);

            String path = "img.jpg";
            File outputFile = new File(path);
            ImageIO.write(bufImg, "jpg", outputFile);
            thresholding();
            frameAvailable = false;
            System.out.println("[Camera] Write new image");
            sendImg();
        }
    }

    public void thresholding(){
        System.out.println("[Camera] Thresholding");
        Mat img = Imgcodecs.imread("img.jpg");
        Mat imgGrey = new Mat();
        Mat imgRes = new Mat();
        Imgproc.cvtColor(img, imgGrey, Imgproc.COLOR_BGR2GRAY);
        Imgproc.threshold(imgGrey, imgRes, 0, 255, 3);
        Imgcodecs.imwrite("img_.jpg", imgRes);
    }

    public void sendImg() throws IOException {

        String path = "img_.jpg";
        File outputFile = new File(path);
        BufferedImage img = ImageIO.read(outputFile);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ImageIO.write(img, "jpg", baos);
        byte[] bytes = baos.toByteArray();
        serverOut.write(bytes);
        System.out.println("[Client] Sent new image");

    }

    public void run() {
        System.out.println("[Camera] Starting thread");
        try {
            connect(6060, 6061);
            getImg();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
